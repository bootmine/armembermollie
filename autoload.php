<?php 
if (is_ssl()) {
    define('ARM_MOLLIE_URL', str_replace('http://', 'https://', WP_PLUGIN_URL . '/' . ARM_MOLLIE_DIR_NAME));
} else {
    define('ARM_MOLLIE_URL', WP_PLUGIN_URL . '/' . ARM_MOLLIE_DIR_NAME);
}
if(!defined('ARMADDON_STORE_URL')){
    define('ARMADDON_STORE_URL', 'https://www.armemberplugin.com/' );
}    
define('ARM_MOLLIE_TEXTDOMAIN','ARM_MOLLIE');

define( 'ARM_MOLLIE_CORE_DIR', ARM_MOLLIE_DIR . '/core/' );
define( 'ARM_MOLLIE_CLASSES_DIR', ARM_MOLLIE_CORE_DIR . 'classes/' );
define( 'ARM_MOLLIE_VIEWS_DIR', ARM_MOLLIE_CORE_DIR . 'views/' );

global $arm_mollie_version;
$arm_mollie_version = '3.5';

global $armnew_mollie_version;

global $wp_version;

class ARM_Mollie{
    
    var $ARM_Mollie_API_KEY;
    
    function __construct(){
        global $arm_payment_gateways;
        if(!empty($arm_payment_gateways) && !empty($arm_payment_gateways->currency))
        {
            $arm_payment_gateways->currency['mollie'] = $this->arm_mollie_currency_symbol();
        }
        
        add_action('init', array(&$this, 'arm_mollie_db_check'));

        register_activation_hook(ARM_MOLLIE_DIR.'/armembermollie.php', array('ARM_Mollie', 'install'));

        register_activation_hook(ARM_MOLLIE_DIR.'/armembermollie.php', array('ARM_Mollie', 'arm_mollie_check_network_activation'));

        register_uninstall_hook(ARM_MOLLIE_DIR.'/armembermollie.php', array( 'ARM_Mollie', 'uninstall'));
        
        add_action('admin_notices', array(&$this, 'arm_mollie_admin_notices'));

        add_action('plugins_loaded', array(&$this, 'arm_mollie_load_textdomain'));
        
        //if(!empty($this->armmollie_license_status())){
        
            add_filter('arm_get_payment_gateways', array(&$this, 'arm_add_mollie_payment_gateways'));
            
            add_filter('arm_get_payment_gateways_in_filters', array(&$this,'arm_add_mollie_payment_gateways'));
            
            add_filter('arm_change_payment_gateway_tooltip', array(&$this, 'arm_change_payment_gateway_tooltip_func'), 10, 3);
            
            add_filter('arm_allowed_payment_gateways', array(&$this, 'arm_payment_allowed_gateways'), 10, 3);

            add_filter('arm_after_payment_gateway_listing_content', array($this, 'arm_after_payment_gateway_listing_content_func'), 10, 3);
            
            add_filter('arm_filter_gateway_names', array(&$this, 'arm_filter_gateway_names_func'), 10);
            
            add_filter('arm_change_pending_gateway_outside', array($this, 'arm_change_pending_gateway_outside'), 10, 3);
            
            add_filter('arm_currency_support',array(&$this,'arm_mollie_currency_support'), 10, 2);
            
            add_action('admin_enqueue_scripts', array(&$this, 'admin_enqueue_script'), 10);
            
            add_action('wp_head', array(&$this, 'arm_mollie_set_front_js'), 10);
            
            add_action('admin_init', array(&$this, 'upgrade_data_mollie'));
                    
            add_filter('arm_change_coupon_code_outside_from_mollie',array(&$this,'arm_mollie_modify_coupon_code'),10,5);    
            
            add_filter('arm_change_pending_gateway_outside',array(&$this,'arm_mollie_change_pending_gateway_outside'),100,3);
            
            add_filter('arm_default_plan_array_filter', array(&$this, 'arm2_default_plan_array_filter_func'), 10, 1);
            
            add_filter('arm_membership_update_user_meta_from_outside', array(&$this, 'arm2_membership_mollie_update_usermeta'), 10, 5);
            
            add_action('arm_update_user_meta_after_renew_outside', array(&$this, 'arm2_mollie_update_meta_after_renew'), 10, 4);    
            
            add_action('arm_payment_gateway_validation_from_setup', array(&$this, 'arm2_payment_gateway_form_submit_action'), 10, 4);
            
            add_action('arm_cancel_subscription_gateway_action', array(&$this, 'arm2_mollie_cancel_subscription'), 10, 2);

            add_filter('add_common_settings_fields_for_translation', array($this, 'get_common_settings_for_language_translation'), 10, 1);
                
            add_filter('arm_default_common_messages', array($this, 'arm_default_common_messages_for_mollie'), 10, 1);
            
            add_action('wp', array(&$this, 'arm2_mollie_webhook'), 5);
            
            add_filter('arm_setup_show_payment_gateway_notice', array(&$this, 'arm_setup_show_mollie_payment_gateway_notice'), 10, 2);

            add_action('arm_enqueue_js_css_from_outside', array(&$this,'arm_enqueue_mollie_js_css_for_model'),10);

            add_action('arm_on_expire_cancel_subscription', array(&$this, 'arm_cancel_subscription_instant'), 100, 4);

            add_filter('arm_set_gateway_warning_in_plan_with_recurring', array(&$this, 'arm_mollie_subscription_warnings'), 10);

            add_filter('arm_get_recurring_payment_log_data', array(&$this, 'arm_get_mollie_recurring_data'), 10, 4);

            add_filter( 'arm_display_update_card_button_from_outside', array( $this, 'arm_display_update_card_button'), 10, 3 );

            add_filter( 'arm_render_update_card_button_from_outside', array( $this, 'arm_render_update_card_button'), 10, 6 );

            add_filter('arm_change_coupon_code_outside_from_mollie', array(&$this, 'arm_check_mollie_coupon_code_valid'), 10, 5);

            add_action('arm_after_add_transaction', array($this, 'arm_modify_mollie_log_amount'), 10, 1);

            // Mollie pro-rata filter

            add_filter('arm_allow_pro_ration_supported_payment_gateway', array( $this, 'arm_mollie_pro_ration_supported_func'), 10, 1);

        //}    
        add_action('admin_enqueue_scripts', array( &$this, 'arm_mollie_license_scripts' ), 20 );
    }

    // Mollie support pro-rata function
    function arm_mollie_pro_ration_supported_func( $payment_gateways_arr ){
        
        //$arm_payment_gateway = 'mollie';
        //$payment_gateways_arr['allow_auto_debit_trial'] = array_push($payment_gateway,$payment_gateways_arr);
        
        return $payment_gateways_arr;
    }

    function arm_modify_mollie_log_amount($log_data){
        global $wpdb, $ARMember;
        if(!empty($log_data) && ($log_data['arm_amount'] == '0.00') && ($log_data['arm_payment_gateway'] == "mollie") && ($log_data['arm_payment_mode'] == "auto_debit_subscription") && ($log_data['arm_transaction_payment_type'] == "subscription")) {
            
            $wpdb->update($ARMember->tbl_arm_payment_log, array('arm_amount' => 0.10), array('arm_log_id' => $log_data['arm_log_id']));
        }
    }

    function arm_check_mollie_coupon_code_valid($return, $payment_mode, $couponData, $planAmt, $planObj){
        global $ARMember, $wpdb, $arm_global_settings;
        if(!empty($payment_mode) && $payment_mode == "auto_debit_subscription" && empty($couponData['arm_coupon_on_each_subscriptions']))
        {
            $err_invalid_coupon = !empty($arm_global_settings->common_message['arm_invalid_coupon']) ? $arm_global_settings->common_message['arm_invalid_coupon'] : __('Coupon code is not valid', 'ARM_MOLLIE');

            $return['status'] = 'error';
            $return['message'] = $err_invalid_coupon;
            $return['validity'] = 'invalid_coupon';
            $return['coupon_amt'] = 0;
            $return['total_amt'] = 0;
            $return['discount'] = 0;
            $return['discount_type'] = '';
        }
        return $return;
    }

    function arm_display_update_card_button( $display, $pg, $planData ){
        if( 'mollie' == $pg ){
            $display = true;
        }
        return $display;
    }
    
    
    function arm_render_update_card_button(  $content, $pg, $planData, $user_plan, $arm_disable_button, $update_card_text ){
        if( 'mollie' == $pg ){
            $content .= '';
        }
        return $content;
    }

    function arm_load_mollie_lib()
    {
        $mollie = "";
        if (file_exists(ARM_MOLLIE_DIR . "/lib/vendor/autoload.php")) {
            require_once  ARM_MOLLIE_DIR . "/lib/vendor/autoload.php";
            $mollie = new \Mollie\Api\MollieApiClient();
        }
        return $mollie;
    }

    function arm_get_mollie_recurring_data($payLog, $subscription_id, $payment_gateway, $arm_payment_gateway_response)
    {
        if($payment_gateway == "mollie")
        {
            global $arm_debug_payment_log_id, $ARMember, $wpdb;

            $arm_mollie_recurring_log_data = array(
                'subscription_id' => $subscription_id,
                'payment_gateway_res' => maybe_serialize($arm_payment_gateway_response)
            );

            do_action('arm_payment_log_entry', 'mollie', 'Get Autodebit payment log data', 'armember', $arm_mollie_recurring_log_data, $arm_debug_payment_log_id);

            $arm_customer_id = !empty($arm_payment_gateway_response['customer_id']) ? $arm_payment_gateway_response['customer_id'] : '';
            if(!empty($arm_customer_id) && $subscription_id)
            {
                $payLog = $wpdb->get_row("SELECT `arm_log_id`, `arm_user_id`, `arm_plan_id`, `arm_token`, `arm_amount`, `arm_currency`, `arm_payer_email`, `arm_extra_vars`, `arm_first_name`, `arm_last_name`, `arm_coupon_code`, `arm_coupon_discount`, `arm_coupon_discount_type`, `arm_coupon_on_each_subscriptions` FROM `" . $ARMember->tbl_arm_payment_log . "` WHERE `arm_token`= '".$subscription_id."' OR `arm_transaction_id`='".$subscription_id."' ORDER BY `arm_log_id` DESC"); //phpcs:ignore
                
                do_action('arm_payment_log_entry', 'mollie', 'Get Autodebit payment log data after transaction check', 'armember', $payLog, $arm_debug_payment_log_id);
                
                if(empty($payLog))
                {
                    $payLog = $this->arm_get_mollie_payment_log_from_subscription($arm_customer_id, $subscription_id);
                    
                    do_action('arm_payment_log_entry', 'mollie', 'Get Autodebit payment log data after mollie api check', 'armember', $payLog, $arm_debug_payment_log_id);
                }
            }
        }
        return $payLog;
    }


    function arm_cancel_subscription_instant($user_id, $plan, $cancel_plan_action, $planData)
    {
        global $wpdb, $ARMember, $arm_global_settings, $arm_subscription_plans, $arm_member_forms, $arm_payment_gateways, $arm_manage_communication;

        $plan_id = $plan->ID;

        $arm_cancel_subscription_data = array();
        $arm_cancel_subscription_data = apply_filters('arm_gateway_cancel_subscription_data', $arm_cancel_subscription_data, $user_id, $plan_id, 'mollie', 'id', '', '');

        $planData = !empty($arm_cancel_subscription_data['arm_plan_data']) ? $arm_cancel_subscription_data['arm_plan_data'] : array();
        $arm_user_payment_gateway = !empty($planData['arm_user_gateway']) ? $planData['arm_user_gateway'] : '';

        if(strtolower($arm_user_payment_gateway) == "mollie")
        {
            $arm_payment_mode = !empty($arm_cancel_subscription_data['arm_payment_mode']) ? $arm_cancel_subscription_data['arm_payment_mode'] : 'manual_subscription';

            $arm_payment_log_data = $arm_cancel_subscription_data['arm_payment_log_data'];

            $arm_subscr_id = !empty($arm_cancel_subscription_data['arm_subscr_id']) ? $arm_cancel_subscription_data['arm_subscr_id'] : '';

            $arm_customer_id = !empty($arm_cancel_subscription_data['arm_customer_id']) ? $arm_cancel_subscription_data['arm_customer_id'] : '';
            if(empty($arm_customer_id))
            {
                $arm_customer_id = $arm_payment_log_data->arm_token;
            }

            $arm_transaction_id = !empty($arm_cancel_subscription_data['arm_transaction_id']) ? $arm_cancel_subscription_data['arm_transaction_id'] : '';
            if(empty($arm_transaction_id))
            {
                $arm_transaction_id = !empty($arm_payment_log_data->arm_transaction_id) ? $arm_payment_log_data->arm_transaction_id : '';
            }

            if(strpos($arm_customer_id, "cst_") == false)
            {
                $arm_customer_id = $this->arm_get_customer_id_from_transaction($arm_transaction_id);
            }

            if($arm_payment_mode == "auto_debit_subscription"){
                $cancel_subscription = $this->arm_cancel_subscription_immediately($arm_subscr_id, $arm_customer_id);
            }
        }
    }

    function arm_cancel_subscription_immediately($arm_subscr_id, $arm_customer_id)
    {
        global $wpdb, $ARMember, $arm_subscription_cancel_msg, $arm_global_settings, $arm_debug_payment_log_id;

        $arm_mollie = $this->arm_load_mollie_lib();

        $gateway_options = get_option('arm_payment_gateway_settings');
        $pgoptions = maybe_unserialize($gateway_options);
        $pgoptions = $pgoptions['mollie'];

        $is_sandbox_mode = ($pgoptions['mollie_payment_mode'] == "sandbox") ? 1 : 0;
        $ARM_Mollie_API_KEY = ( $is_sandbox_mode ) ? $pgoptions['mollie_sandbox_api_key'] : $pgoptions['mollie_api_key'];
        $arm_mollie->setApiKey($ARM_Mollie_API_KEY);
        $cancel_subscription = '';

        try {
            $get_subscriptions_customer = $arm_mollie->customers->get($arm_customer_id);
            $get_subscriptions = $get_subscriptions_customer->getSubscription($arm_subscr_id);
            do_action('arm_payment_log_entry', 'mollie', 'Get subscription details while cancel subscription', 'payment_gateway', json_encode($get_subscriptions), $arm_debug_payment_log_id);
        } catch (Exception $e) {      
            if(!empty($e->getMessage()))
            {
                $arm_err_msg = $e->getMessage();
                do_action('arm_payment_log_entry', 'mollie', 'Error in getting subscription details while cancel subscription ', 'payment_gateway', $arm_err_msg, $arm_debug_payment_log_id);
            }
        }

        if($get_subscriptions->status != 'canceled'){
            try {
                $get_subscriptions_customer = $arm_mollie->customers->get($arm_customer_id);
                $cancel_subscription = $get_subscriptions_customer->cancelSubscription($arm_subscr_id);
                do_action('arm_payment_log_entry', 'mollie', 'Cancel subscription response', 'payment_gateway', json_encode($cancel_subscription), $arm_debug_payment_log_id);
            } catch (Exception $e) {
                if(!empty($e->getMessage()))
                {
                    $arm_subscription_cancel_msg = __("Error in cancel subscription from Mollie Account.", "ARM_MOLLIE")." ".$e->getMessage();
                    do_action('arm_payment_log_entry', 'mollie', 'Error in getting subscription cancel', 'payment_gateway', $e->getMessage(), $arm_debug_payment_log_id);
                }
                else
                {
                    $common_messages = isset($arm_global_settings->common_message) ? $arm_global_settings->common_message : array();
                    $arm_subscription_cancel_msg = isset($common_messages['arm_payment_gateway_subscription_failed_error_msg']) ? $common_messages['arm_payment_gateway_subscription_failed_error_msg'] : __("Membership plan couldn't cancel. Please contact the site administrator.", 'ARM_MOLLIE');
                }
            }
        }

        return $cancel_subscription;
    }

    function arm_mollie_subscription_warnings($notice) {
        // if need to display any notice related subscription in Add / Edit plan page
        if ($this->is_version_compatible()){
            $notice .= "<span style='margin-bottom:10px;'><b>". __('Mollie (if Mollie payment gateway is enabled)','ARM_MOLLIE')."</b><br/>";
            $notice .= "<ol style='margin-left:30px;'>";
            $notice .= "<li>".__('For the Auto-debit Payment method, Mollie supports a maximum interval of 1 year. Like 12 months, 52 weeks, or 365 days billing cycle.','ARM_MOLLIE')."</li>";
            $notice .= "</ol>";
            $notice .= "</span>";
        } 
        return $notice;
    }
    
    function arm_setup_show_mollie_payment_gateway_notice( $gateway_note, $payment_gateway ) {
        if($payment_gateway == 'mollie') {
            global $arm_payment_gateways;
            $currency = $arm_payment_gateways->arm_get_global_currency();
            $gateway_note = '<span class="arm_invalid" id="arm_mollie_warning" style="width:254px;">'.__("NOTE : In case of automatic subscription, if final payable amount will be zero, then also user need to pay minimum 0.10", 'ARM_MOLLIE').' '.$currency.' '.__("amount to start subscription for mollie.", 'ARM_MOLLIE').'</span>';
        }
        return $gateway_note;
    }
    
    function arm2_default_plan_array_filter_func( $default_plan_array ) {
        $default_plan_array['arm_mollie'] = array();
        return $default_plan_array;
    }
    
    function arm2_membership_mollie_update_usermeta($posted_data, $user_id, $plan, $log_detail, $pgateway) {
        if ($pgateway == 'mollie') {
            $posted_data['arm_mollie'] = array('sale_id' => $log_detail->arm_token, 'transaction_id' => $log_detail->arm_transaction_id);
        }
        return $posted_data;
    }
    
    function arm2_mollie_update_meta_after_renew($user_id, $log_detail, $plan_id, $payment_gateway) {
        global $ARMember;
        if ($payment_gateway == 'mollie') {
            if ($user_id != '' && !empty($log_detail) && $plan_id != '' && $plan_id != 0) {
                global $arm_subscription_plans;
                $defaultPlanData = $arm_subscription_plans->arm_default_plan_array();
                $plan_data = get_user_meta($user_id, 'arm_user_plan_' . $plan_id, true);
                $plan_data = !empty($plan_data) ? $plan_data : array();
                $plan_data = shortcode_atts($defaultPlanData, $plan_data);
                $pg_subsc_data = array('sale_id' => $log_detail->arm_token, 'transaction_id' => $log_detail->arm_transaction_id);
                $plan_data['arm_2checkout'] = '';
                $plan_data['arm_authorize_net'] = '';
                $plan_data['arm_stripe'] = '';
                $plan_data['arm_mollie'] = $pg_subsc_data;
                
                update_user_meta($user_id, 'arm_user_plan_' . $plan_id, $plan_data);
            }
        }
    }
    
    function arm_mollie_set_front_js($force_enqueue = false){
        if( $this->is_version_compatible() ){
            global $ARMember, $arm_mollie_version;
            $is_arm_front_page = $ARMember->is_arm_front_page();
            if ($is_arm_front_page === TRUE || $force_enqueue == TRUE){
                wp_register_script('arm_mollie_front_js', ARM_MOLLIE_URL . '/js/arm_front_mollie.js', array(), $arm_mollie_version);
                wp_enqueue_script('arm_mollie_front_js');
            }
        }
    }

    function arm_enqueue_mollie_js_css_for_model(){
        $this->arm_mollie_set_front_js(true);
    }
    
    function upgrade_data_mollie() {
            global $armnew_mollie_version;
    
            if (!isset($armnew_mollie_version) || $armnew_mollie_version == "")
                $armnew_mollie_version = get_option('arm_mollie_version');
    
            if (version_compare($armnew_mollie_version, '3.5', '<')) {
                $path = ARM_MOLLIE_VIEWS_DIR . '/upgrade_latest_data_mollie.php';
                include($path);
            }
        }
        
    function arm_mollie_load_textdomain() {
        load_plugin_textdomain('ARM_MOLLIE', false, dirname( plugin_basename(__FILE__) ) . '/languages/');
    }
    
    public static function arm_mollie_db_check() {
        global $arm_mollie; 
        $arm_mollie_version = get_option('arm_mollie_version');

        if (!isset($arm_mollie_version) || $arm_mollie_version == '')
            $arm_mollie->install();
    }
    
    public static function install() {
        global $arm_mollie;
        $arm_mollie_version = get_option('arm_mollie_version');

        if (!isset($arm_mollie_version) || $arm_mollie_version == '') {

            global $wpdb, $arm_mollie_version;

            update_option('arm_mollie_version', $arm_mollie_version);
        }
    }
    
    /*
     * Restrict Network Activation
     */
    public static function arm_mollie_check_network_activation($network_wide) {
        if (!$network_wide)
            return;

        deactivate_plugins(plugin_basename(__FILE__), TRUE, TRUE);

        header('Location: ' . network_admin_url('plugins.php?deactivate=true'));
        exit;
    }
    public static function uninstall() {
        delete_option('arm_mollie_version');
    }
   
    function arm_mollie_currency_symbol() {
        $currency_symbol = array(
            'EUR' => '&#128;',
            'GBP' => '&#163;',
            'DKK' => '&nbsp;&#107;&#114;',
            'NOK' => '&nbsp;&#107;&#114;',
            'PLN' => '&#122;&#322;',
            'SEK' => '&nbsp;&#107;&#114;',
            'CHF' => '&#67;&#72;&#70;',
            'USD' => '$',
        );
        return $currency_symbol;
    }
    
    function arm_add_mollie_payment_gateways($default_payment_gateways) {
        if ($this->is_version_compatible()){
            global $arm_payment_gateways;
            $default_payment_gateways['mollie']['gateway_name'] = __('Mollie', 'ARM_MOLLIE');
            return $default_payment_gateways;
        }
        else
        {
            return $default_payment_gateways;
        }
    }
    
    function arm_mollie_admin_notices(){
        global $pagenow, $arm_slugs;    
        if($pagenow == 'plugins.php' || (isset($_REQUEST['page']) && in_array($_REQUEST['page'], (array) $arm_slugs))){
            $armmollie_license_status=get_option('armmollie_license_status');
            $armmollie_license_key=get_option('armmollie_license_key');
            $armmollie_page_slug=(isset($_REQUEST['page']))?sanitize_text_field($_REQUEST['page']):'';
            if(($armmollie_license_status!='valid' || empty($armmollie_license_key)) && $armmollie_page_slug!='arm_manage_license' && $this->is_armember_support()){
                $admin_doc_license_url = "https://www.armemberplugin.com/documents/activate-armember-addon-license/";
                printf( "<div class='notice notice-error arf-notice-update-warning is-dismissible' style='border-left-color:#dc3232;display:block;'><p><b>" . esc_html__("Please activate the ARMember - Mollie payment gateway Addon License from ARMember ➝ Licensing page to get future updates. For more information, %1\$s click here %2\$s.", 'ARM_MOLLIE') . "</b></p></div>","<a href='".esc_url( $admin_doc_license_url )."' target='_blank'>",'</a>');
            }else if(!$this->is_armember_support())
                echo "<div class='updated updated_notices'><p>" . esc_html__('Mollie For ARMember plugin requires ARMember Plugin installed and active.', 'ARM_MOLLIE') . "</p></div>";

            else if (!$this->is_version_compatible())
                echo "<div class='updated updated_notices'><p>" . esc_html__('Mollie For ARMember plugin requires ARMember plugin installed with version 4.4 or higher.', 'ARM_MOLLIE') . "</p></div>";
        }
    }
    
    function is_armember_support() {

        include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

        return is_plugin_active('armember/armember.php');
    }
    
    function get_armember_version(){
        $arm_db_version = get_option('arm_version');
        
        return (isset($arm_db_version)) ? $arm_db_version : 0;
    }
    
    function is_version_compatible(){
        if (!version_compare($this->get_armember_version(), '4.4', '>=') || !$this->is_armember_support()) :
            return false;
        else : 
            return true;
        endif;
    }
    
    function arm_change_payment_gateway_tooltip_func($titleTooltip, $gateway_name, $gateway_options) {
        if ($gateway_name == 'mollie') {
            return " You can find API Key in your mollie account. To get more details, Please refer this <a href='https://www.mollie.com/en/docs/authentication'>document</a>.";
        }
        return $titleTooltip;
    }
    
    function arm_filter_gateway_names_func($pgname) {
        $pgname['mollie'] = esc_html__('Mollie', 'ARM_MOLLIE');
        return $pgname;
    }
    
    function arm_payment_allowed_gateways($allowed_gateways, $plan_obj, $plan_options) {
        $allowed_gateways['mollie'] = "1";
        return $allowed_gateways;
    }
    
    function admin_enqueue_script(){
        $arm_mollie_page_array = array('arm_general_settings', 'arm_membership_setup');
        $arm_mollie_action_array = array('payment_options', 'new_setup', 'edit_setup');
        if ($this->is_version_compatible() && isset($_REQUEST['page']) && isset($_REQUEST['action'])){
            if(in_array($_REQUEST['page'], $arm_mollie_page_array) && in_array($_REQUEST['action'], $arm_mollie_action_array))
            {
                global $arm_mollie_version;
                wp_register_script( 'arm-admin-mollie', ARM_MOLLIE_URL . '/js/arm_admin_mollie.js', array(), $arm_mollie_version );
                wp_enqueue_script( 'arm-admin-mollie' );
            }
        }
    }
    function armmollie_license_status(){
        $armmollie_license_status=get_option('armmollie_license_status');
        return ($armmollie_license_status=='valid')?true:false;
    }
    function arm_mollie_license_scripts(){
        global $arm_mollie_version, $arm_version;
        wp_register_script('arm-mollie-license', ARM_MOLLIE_URL . '/js/arm_mollie_license.js', array(), $arm_mollie_version);
        if( isset($_REQUEST['page']) && $_REQUEST['page'] == 'arm_manage_license')
        {
            wp_enqueue_script( 'arm-mollie-license');
            global $wp_styles;
            $srcs = array_map('basename', (array) wp_list_pluck($wp_styles->registered, 'src') );
            if (!in_array('arm_mollie_admin.css', $srcs)) {
                wp_register_style( 'arm_mollie_css', ARM_MOLLIE_URL . '/css/arm_mollie_admin.css', array(), $arm_mollie_version );
            }    
            wp_enqueue_style( 'arm_mollie_css' );
        }
    }
    
    function arm_after_payment_gateway_listing_content_func($payment_gateway_content, $gateway_name, $gateway_options){
        global $arm_global_settings;
        if ($gateway_name == 'mollie') {      
            $gateway_options['mollie_payment_mode'] = (!empty($gateway_options['mollie_payment_mode']) ) ? $gateway_options['mollie_payment_mode'] : 'sandbox';
            $gateway_options['status'] = isset($gateway_options['status']) ? $gateway_options['status'] : 0;
            $disabled_field_attr = ($gateway_options['status'] == '1') ? '' : 'disabled="disabled"';
            $readonly_field_attr = ($gateway_options['status'] == '1') ? '' : 'readonly="readonly"';

            $arm_mollie_is_sandbox_checked = ($gateway_options['mollie_payment_mode'] == 'sandbox') ? "checked='checked'" : '';
            $arm_mollie_is_live_checked = ($gateway_options['mollie_payment_mode'] == 'live') ? "checked='checked'" : '';
            
            $arm_mollie_gateway_html = '<tr class="form-field">
                <th class="arm-form-table-label"><label>'. esc_html__('Payment Mode', 'ARM_MOLLIE').' *</label></th>
                <td class="arm-form-table-content">
                    <input id="arm_mollie_payment_gateway_mode_sand" class="arm_general_input arm_mollie_mode_radio arm_iradio arm_active_payment_'. strtolower($gateway_name).'" type="radio" value="sandbox" name="payment_gateway_settings[mollie][mollie_payment_mode]" '.$arm_mollie_is_sandbox_checked.' '. $disabled_field_attr.'>
                    <label for="arm_mollie_payment_gateway_mode_sand">'. esc_html__('Sandbox', 'ARM_MOLLIE').'</label>
                    <input id="arm_mollie_payment_gateway_mode_pro" class="arm_general_input arm_mollie_mode_radio arm_iradio arm_active_payment_'. strtolower($gateway_name).'" type="radio" value="live" name="payment_gateway_settings[mollie][mollie_payment_mode]" '.$arm_mollie_is_live_checked.' '.  $disabled_field_attr .'>
                    <label for="arm_mollie_payment_gateway_mode_pro">'. esc_html__('Live', 'ARM_MOLLIE').'</label>
                </td>
            </tr>';
            $mollie_hidden = "hidden_section";
            if (isset($gateway_options['mollie_payment_mode']) && $gateway_options['mollie_payment_mode'] == 'sandbox') {
                $mollie_hidden = "";
            }else if(!isset($gateway_options['mollie_payment_mode'])){
                $mollie_hidden = "";
            }
            $arm_mollie_sandbox_api_key = (!empty($gateway_options['mollie_sandbox_api_key'])) ? $gateway_options['mollie_sandbox_api_key'] : '';
            $arm_mollie_gateway_html .= '<tr class="form-field arm_mollie_sandbox_fields '. esc_attr($mollie_hidden).' ">
                <th class="arm-form-table-label"><label>'. esc_html__('Sandbox API Key', 'ARM_MOLLIE').' *</label></th>
                <td class="arm-form-table-content">
                    <input type="text" class="arm_active_payment_'. strtolower($gateway_name).'" id="arm_mollie_sandbox_api_key" name="payment_gateway_settings[mollie][mollie_sandbox_api_key]" value="'. esc_attr($arm_mollie_sandbox_api_key) .'" '. $readonly_field_attr.' />
                </td>
            </tr>';
            $mollie_live_fields = "hidden_section";
            if (isset($gateway_options['mollie_payment_mode']) && $gateway_options['mollie_payment_mode'] == "live") {
                $mollie_live_fields = "";
            }
            $arm_mollie_live_api_key = (!empty($gateway_options['mollie_api_key'])) ? $gateway_options['mollie_api_key'] : '';
            $arm_mollie_gateway_html .= '<tr class="form-field arm_mollie_fields '. esc_attr($mollie_live_fields).' ">
                <th class="arm-form-table-label"><label>'. esc_html__('Live API Key', 'ARM_MOLLIE').' *</label></th>
                <td class="arm-form-table-content">
                    <input type="text" class="arm_active_payment_'. strtolower($gateway_name).'" id="arm_mollie_live_api_key" name="payment_gateway_settings[mollie][mollie_api_key]" value="'.esc_attr($arm_mollie_live_api_key).'" '. $readonly_field_attr.' />
                </td>
            </tr>';
            if(version_compare($this->get_armember_version(), '2.0', '>=')) { 
                $arm_mollie_gateway_html .= '<tr class="form-field arm_mollie_fields">
                    <th class="arm-form-table-label"><label>'. esc_html__('Cancel URL', 'ARM_MOLLIE').' *</label></th>
                    <td class="arm-form-table-content">';
                        $page = get_page_by_title('Cancel Payment', OBJECT, 'page'); 
                        $page_url = !empty(get_permalink($page->ID)) ? get_permalink($page->ID) : '';
                        $arm_mollie_cancel = (!empty($gateway_options['cancel_url'])) ? $gateway_options['cancel_url'] : $page_url;
                        $arm_mollie_gateway_html .= '<input type="text" class="arm_active_payment_'. strtolower($gateway_name).'" id="arm_mollie_live_api_key" name="payment_gateway_settings[mollie][cancel_url]" value="'. esc_url($arm_mollie_cancel) .'"/>
                    </td>
                </tr>
                <tr class="form-field">
                    <th class="arm-form-table-label"><label>'. esc_html__('Language', 'ARM_MOLLIE').'</label></th>
                    <td class="arm-form-table-content">';
                        $arm_mollie_language = $this->arm_mollie_language();
                        $arm_mollie_selected_lang = (!empty($gateway_options['language'])) ? $gateway_options['language'] : 'en';
                        $arm_mollie_lang_css = ($gateway_options['status']=='1') ? '' : 'style="border:1px solid #DBE1E8"';
                        $arm_mollie_gateway_html .= '<input type="hidden" id="arm_mollie_language" name="payment_gateway_settings[mollie][language]" value="'. esc_attr($arm_mollie_selected_lang).'" />
                        <dl class="arm_selectbox arm_active_payment_'. strtolower($gateway_name).'" '. $disabled_field_attr.'>
                            <dt '. $arm_mollie_lang_css .'><span></span><input type="text" style="display:none;" value="'. esc_html__('English ( en )', 'ARM_MOLLIE').'" class="arm_autocomplete"/><i class="armfa armfa-caret-down armfa-lg"></i></dt>
                            <dd>
                                <ul data-id="arm_mollie_language">';
                                    foreach ($arm_mollie_language as $key => $value):
                                        $arm_mollie_gateway_html .= '<li data-label="'. $value . " ( $key ) " .'" data-value="'. esc_attr($key).'">'. $value . " ( $key ) ".'</li>';
                                    endforeach;
                                    $arm_mollie_gateway_html .= '</ul>
                            </dd>
                        </dl>
                    </td>
                </tr>';
            }
            $payment_gateway_content .= $arm_mollie_gateway_html;
        }
        return $payment_gateway_content;
    }

    function get_common_settings_for_language_translation($settings_array){

        $settings_array["Payment Related Messages"]["arm_payment_fail_mollie"] = esc_html__('Payment Fail (Mollie)', 'ARM_MOLLIE');

        return $settings_array;

    }
    function arm_default_common_messages_for_mollie($settings_array){
        
        $settings_array["arm_payment_fail_mollie"] = esc_html__('Sorry something went wrong while processing payment with Mollie.', 'ARM_MOLLIE');
        
        return $settings_array;

    }
    
    function arm_mollie_language(){
        $currency_symbol = array(
            'be' => __('Belarusian', 'ARM_MOLLIE'),
            'be-fr' => __('Belgium / French)', 'ARM_MOLLIE'),
            'nl' => __('Dutch', 'ARM_MOLLIE'),
            'en' => __('English', 'ARM_MOLLIE'),
            'de' => __('German', 'ARM_MOLLIE'),
            'fr' => __('French', 'ARM_MOLLIE'),
            'es' => __('Spanish', 'ARM_MOLLIE'),
        );
        return $currency_symbol;
    }
    
    
    function arm2_payment_gateway_form_submit_action($payment_gateway, $payment_gateway_options, $posted_data, $entry_id = 0) {
        
        global $wpdb, $ARMember, $arm_global_settings, $arm_subscription_plans, $arm_member_forms, $arm_manage_coupons, $payment_done, $arm_payment_gateways, $arm_transaction, $arm_membership_setup, $is_free_manual, $arm_debug_payment_log_id;

        $arm_mollie = $this->arm_load_mollie_lib();

        $arm_return_data = array();
        $arm_return_data = apply_filters('arm_calculate_payment_gateway_submit_data', $arm_return_data, $payment_gateway, $payment_gateway_options, $posted_data, $entry_id);

        $all_payment_gateways = $arm_payment_gateways->arm_get_active_payment_gateways();
        if ($payment_gateway == 'mollie' && isset($all_payment_gateways['mollie']) && !empty($all_payment_gateways['mollie'])) {
            $is_free_manual = false;
            $currency = $arm_payment_gateways->arm_get_global_currency();
        
            $entry_data = !empty($arm_return_data) ? $arm_return_data['arm_entry_data'] : '';
            if (!empty($entry_data))
            {    
                $user_email_add = $arm_return_data['arm_user_email'];
                $form_id = $entry_data['arm_form_id'];
                $user_id = $entry_data['arm_user_id'];

                $entry_values = maybe_unserialize($entry_data['arm_entry_value']);
                $payment_cycle = $entry_values['arm_selected_payment_cycle']; 

                $tax_data = !empty($arm_return_data['arm_tax_data']) ? $arm_return_data['arm_tax_data'] : array();

                $tax_percentage = !empty($tax_data['tax_percentage']) ? $tax_data['tax_percentage'] : 0;

                $arm_user_old_plan = (isset($entry_values['arm_user_old_plan']) && !empty($entry_values['arm_user_old_plan'])) ? explode(",",$entry_values['arm_user_old_plan']) : array();

                $setup_id = (isset($entry_values['setup_id']) && !empty($entry_values['setup_id'])) ? $entry_values['setup_id'] : 0 ;
                
                $user_name = '';
                if (is_user_logged_in()) {
                    $user_obj = get_user_by( 'ID', $user_id);
                    $user_name = $user_obj->first_name." ".$user_obj->last_name;
                    $user_email_add = $user_obj->user_email;
                }else { 
                    $user_name = $entry_values['first_name']." ".$entry_values['last_name'];
                }
                $user_name = trim($user_name);
                
                $plan_id = !empty($arm_return_data['arm_plan_id']) ? $arm_return_data['arm_plan_id'] : 0;
                $plan = !empty($arm_return_data['arm_plan_obj']) ? $arm_return_data['arm_plan_obj'] : new ARM_Plan($plan_id);
                
                $payment_mode_ = !empty($posted_data['arm_payment_mode']['mollie']) ? $posted_data['arm_payment_mode']['mollie'] : 'both';
               
                $recurring_payment_mode = !empty($arm_return_data['arm_payment_mode']) ? $arm_return_data['arm_payment_mode'] : 'manual_subscription';
                
                $plan_action = !empty($arm_return_data['arm_plan_action']) ? $arm_return_data['arm_plan_action'] : 'new_subscription';

                $plan_payment_type = $plan->payment_type;
                $is_recurring = $plan->is_recurring();
                $plan_name = !empty($plan->name) ? $plan->name : "Plan Name";
                $amount = !empty($plan->amount) ? $plan->amount : "0";

                if($is_recurring) {
                    $recurring_data = $arm_return_data['arm_recurring_data'];
                    $amount = $recurring_data['amount'];
                } else {
                    $amount = !empty($plan->amount) ? $plan->amount : 0;
                }
                $amount = str_replace(',','',$amount);
                $discount_amt = $amount;

                $coupon_amount = 0;
                $arm_is_trial = 0;
                $is_trial = false;
                $allow_trial = true;
                if (is_user_logged_in()) {
                    $user_id = get_current_user_id();
                    $user_plan = get_user_meta($user_id, 'arm_user_plan_ids', true);
                    if(!is_array($user_plan)){
                        $user_plan = explode(',', $user_plan);
                    }
                    $user_plan_id = $user_plan;
                    if (in_array($plan->ID, $user_plan)) {
                        $allow_trial = false;
                    }
                }

                if($is_recurring) {
                    if (!empty($arm_return_data['arm_recurring_data']['trial']) && !empty($arm_return_data['arm_trial_data']) && $allow_trial) {
                        $is_trial = true;
                        $arm_is_trial = '1';

                        $trial_data = !empty($arm_return_data['arm_trial_data']) ? $arm_return_data['arm_trial_data'] : $arm_return_data['arm_recurring_data']['trial'];
                        if(!empty($trial_data))
                        {
                            $discount_amt = $trial_data['amount'];
                            $trial_period = $trial_data['period'];
                            $trial_interval = $trial_data['interval'];
                        }
                    }
                }

                $coupon_amount = $arm_coupon_discount = 0;
                $arm_coupon_discount_type = '';
                $extraFirstParam = array();
                if ($arm_return_data['arm_coupon_data']) {
                    $couponApply = $arm_return_data['arm_coupon_data'];
                    $coupon_amount = $couponApply['coupon_amt'];
                    $coupon_amount = str_replace(',','',$coupon_amount);

                    $discount_amt = $couponApply['total_amt'];
                    $discount_amt = str_replace(',','',$discount_amt);

                    $arm_coupon_discount = $couponApply['discount'];
                    $arm_coupon_discount_type = ($couponApply['discount_type'] != 'percentage') ? $currency : "%";
                    if (!empty($coupon_amount) && $coupon_amount > 0) {
                        $extraFirstParam['coupon'] = array(
                            'coupon_code' => $couponApply['arm_coupon_code'],
                            'amount' => $coupon_amount,
                        );
                    }
                }
                
                $payment_data = array();
                
                $payment_mode = $payment_gateway_options['mollie_payment_mode'];
                $is_sandbox_mode = $payment_mode == "sandbox" ? true : false;
                $this->ARM_Mollie_API_KEY = ( $is_sandbox_mode ) ? $payment_gateway_options['mollie_sandbox_api_key'] : $payment_gateway_options['mollie_api_key'];
                $arm_mollie_language = isset($payment_gateway_options['language']) ? $payment_gateway_options['language'] : 'en';
                
                $arm_mollie_webhookurl = '';
                $arm_mollie_webhookurl = $arm_global_settings->add_query_arg("arm-listener", "arm_mollie_wh_api", get_home_url() . "/");
                
                $arm_mollie->setApiKey($this->ARM_Mollie_API_KEY);

                //get customer id
                $arm_mollie_customer_id = '';
                if($plan_action == 'renew_subscription'){
                    $arm_entry_tbl = $ARMember->tbl_arm_entries;
                    $arm_entry_data = $wpdb->get_row($wpdb->prepare("SELECT arm_user_id, arm_entry_email FROM `{$arm_entry_tbl}` WHERE `arm_entry_id` = %s ", $entry_data['arm_entry_id'])); //phpcs:ignore
                    
                    $arm_payment_log_tbl = $ARMember->tbl_arm_payment_log;
                    $arm_entry_data = $wpdb->get_row($wpdb->prepare("SELECT arm_token FROM `{$arm_payment_log_tbl}` WHERE `arm_user_id` = %s AND `arm_payer_email` = %s AND `arm_payment_gateway` = %s", $arm_entry_data->arm_user_id , $arm_entry_data->arm_entry_email, 'mollie')); //phpcs:ignore
                    
                    if(isset($arm_entry_data->arm_token) && $arm_entry_data->arm_token != '-')
                    {
                        try{
                            $arm_mollie_customer = $arm_mollie->customers->get($arm_entry_data->arm_token);
                            $arm_mollie_customer_id = isset($arm_mollie_customer->id) ? $arm_mollie_customer->id : '';
                        }catch(Exception $e){
                            $arm_mollie_customer_id = "";
                            
                        }
                    }
                }
                
                if(!isset($arm_mollie_customer_id) || $arm_mollie_customer_id == ''){
                    $arm_mollie_customer_error_msg = "";
                    $customer_details_to_mollie = array(
                        "email"    => $user_email_add
                    );
                    if(!empty($user_name))
                    {
                        $customer_details_to_mollie["name"] = $user_name;
                    }
                    try{
                        $arm_mollie_customer = $arm_mollie->customers->create($customer_details_to_mollie);
                        $arm_mollie_customer_id = $arm_mollie_customer->id;
                        $arm_mollie_customer_error_msg = "";
                    }catch(Exception $e){
                        $arm_mollie_customer_id = "";
                        $arm_mollie_customer_error_msg = $e->getMessage();
                    }

                    if(!empty($arm_mollie_customer_error_msg))
                    {
                        global $payment_done;
                        $payment_done = array('status' => FALSE, 'error' => $arm_mollie_customer_error_msg);
                        return $payment_done;
                    }
                }

                $tax_amount = 0;
                if($tax_percentage > 0){
                    $tax_amount = $arm_return_data['arm_tax_data']['tax_amount'];
                    if($recurring_payment_mode == "manual_subscription"){
                        $discount_amt = $discount_amt + ($discount_amt * ($tax_percentage / 100));
                    }else{
                        $discount_amt = $arm_return_data['arm_tax_data']['tax_final_amount'];;
                    }
                    
                    if($is_trial)
                    {
                        $tax_amount = $arm_return_data['arm_tax_data']['trial_tax_amount'];
                        $discount_amt = $arm_return_data['arm_tax_data']['final_trial_amount'];
                    }
                }
                
                if(($discount_amt <= 0 || $discount_amt == '0.00') && $recurring_payment_mode == 'auto_debit_subscription')
                {
                    $discount_amt = 0.10;
                }

                if(($discount_amt <= 0 || $discount_amt == '0.00') && $recurring_payment_mode == 'manual_subscription')
                {
                    global $payment_done;
                    $mollie_response = array();
                    $current_user_id = 0;
                    if (is_user_logged_in()) {
                        $mollie_response['arm_user_id'] = $user_id;
                    }
                    $arm_first_name = (isset($posted_data['first_name'])) ? $posted_data['first_name']:'';
                    $arm_last_name = (isset($posted_data['last_name'])) ? $posted_data['last_name']:'';
                    if(!empty($user_id)){
                        if(empty($arm_first_name)){
                            $user_detail_first_name = get_user_meta( $user_id, 'first_name', true);
                            $arm_first_name = $user_detail_first_name;
                        }
                        if(empty($arm_last_name)){
                            $user_detail_last_name = get_user_meta( $user_id, 'last_name', true);
                            $arm_last_name = $user_detail_last_name;
                        }    
                    }
                    $mollie_response['arm_plan_id'] = $plan_id;
                    $mollie_response['arm_first_name'] = $arm_first_name;
                    $mollie_response['arm_last_name'] = $arm_last_name;
                    $mollie_response['arm_payment_gateway'] = 'mollie';
                    $mollie_response['arm_payment_type'] = $plan_payment_type;
                    $mollie_response['arm_token'] = '-';
                    $mollie_response['arm_payer_email'] = $user_email_add;
                    $mollie_response['arm_receiver_email'] = '';
                    $mollie_response['arm_transaction_id'] = '-';
                    $mollie_response['arm_transaction_payment_type'] = $plan_payment_type;
                    $mollie_response['arm_transaction_status'] = 'success';
                    $mollie_response['arm_payment_mode'] = 'manual_subscription';
                    $mollie_response['arm_payment_date'] = current_time('mysql');
                    $mollie_response['arm_amount'] = 0;
                    $mollie_response['arm_currency'] = $currency;
                    $mollie_response['arm_coupon_code'] = $posted_data['arm_coupon_code'];
                    $mollie_response['arm_coupon_discount'] = $arm_coupon_discount;
                    $mollie_response['arm_coupon_discount_type'] = $arm_coupon_discount_type;
                    $mollie_response['arm_response_text'] = '';
                    $mollie_response['arm_extra_vars'] = array( 'payment_type'=> 'mollie', 'payment_mode' => $payment_mode );
                    $mollie_response['arm_is_trial'] = $arm_is_trial;
                    $mollie_response['arm_created_date'] = current_time('mysql');
                    
                    $payment_log_id = $arm_payment_gateways->arm_save_payment_log($mollie_response);
                    $return = array('status' => TRUE, 'log_id' => $payment_log_id, 'entry_id' => $entry_id);
                    $payment_done = $return;
                    $is_free_manual = true;
                    do_action('arm_after_mollie_free_payment',$plan,$payment_log_id,$arm_is_trial,$posted_data['arm_coupon_code'],$extraFirstParam);
                    return $return;
                }
                                
                $discount_amt = number_format((float)$discount_amt, 2);
                $discount_amt = str_replace(',', '', $discount_amt);

                $arm_mollie_redirecturl = $arm_return_data['arm_extra_vars']['arm_return_url'];
                $arm_mollie_webhookurl = $arm_return_data['arm_extra_vars']['arm_notification_url'];
                $arm_mollie_checkout_url = "";
                $arm_mollie_cancel_url = $arm_return_data['arm_extra_vars']['arm_cancel_url'];

                if(!empty($all_payment_gateways['mollie']['cancel_url']))
                {
                    $arm_mollie_cancel_url = $all_payment_gateways['mollie']['cancel_url'];
                }
                    
                //customer payment
                if($is_recurring && $recurring_payment_mode == 'auto_debit_subscription')
                {
                    $recur_cycles = "";
                    $recur_interval = "";
                    $recurring_type = "";
                    $recurring_data = $arm_return_data['arm_recurring_data'];
                    
                    $recur_cycles = (!empty($recurring_data['cycles']) && $recurring_data['cycles'] != 'infinite') ? $recurring_data['cycles'] : 'infinite';
                    $recur_interval = $recurring_data['interval'];
                    $recurring_type = (!empty($recurring_data['period'])) ? $recurring_data['period'] : 'Day';

                    $arm_is_mollie_support = 1;

                    if ($recurring_type == "D" || $recurring_type == 'Day') {
                        $recurring_type = $recur_interval." days";
                    } else if ($recurring_type == "W") {
                        $recurring_type = $recur_interval." weeks";
                        if($recur_interval > 52){
                            $arm_is_mollie_support = 0;
                        }
                    } else if ($recurring_type == "M") {
                        $recurring_type = $recur_interval." months";
                        if($recur_interval > 12){
                            $arm_is_mollie_support = 0;
                        }
                    } else if ($recurring_type == "Y") {
                        if($recur_interval == 1 || $recur_interval == '1'){
                            $recurring_type = "12 months";
                        }else{
                            $arm_is_mollie_support = 0;
                        }
                    }

                    if(!$arm_is_mollie_support){
                        $err_msg = __('For the Auto-debit Payment method, Mollie supports a maximum interval of 1 year. Like 12 months, 52 weeks, or 365 days billing cycle.', 'ARM_MOLLIE');
                        $payment_done = array('status' => FALSE, 'error' => $err_msg);
                        return $payment_done;
                    }

                    try{
                        //Setup First Payment
                        $arm_first_payment_params = array(
                            'amount'       => array(
                                'currency' => $currency,
                                'value'    => $discount_amt,
                            ),
                            'sequenceType' => 'first',
                            'description'  => $plan_name,
                            'redirectUrl'  => $arm_mollie_redirecturl,
                            'cancelUrl'    => $arm_mollie_cancel_url,
                            'webhookUrl'   => $arm_mollie_webhookurl,
                            'metadata'     => array(
                                'plan_id'           => $plan_id,
                                'entry_id'          => $entry_id,
                                'user_email'        => $user_email_add,
                                'form_id'           => $form_id,
                                'plan_payment_type' => $plan_payment_type,
                                'cst_id'            => $arm_mollie_customer_id,
                                'recurring_payment' => 1,
                            ),
                        );

                        $arm_mollie_customer_payment = $arm_mollie->customers->get($arm_mollie_customer_id)->createPayment($arm_first_payment_params);
                        $arm_mollie_checkout_url = !empty($arm_mollie_customer_payment->_links->checkout) ? $arm_mollie_customer_payment->_links->checkout->href : '';
                    }catch(Exception $e){
                        $actual_message = $e->getMessage();

                        do_action('arm_payment_log_entry', 'mollie', 'Error while submitting auto-debit form request', 'armember', $actual_message, $arm_debug_payment_log_id);
                    }
                }
                else
                {
                    try{
                        $arm_first_payment_params = array(
                            'amount'       => array(
                                'currency' => $currency,
                                'value'    => $discount_amt,
                            ),
                            'customerId'    => $arm_mollie_customer_id,
                            'description'   => $plan_name,
                            'redirectUrl'   => $arm_mollie_redirecturl,
                            'cancelUrl'   => $arm_mollie_cancel_url,
                            'webhookUrl'   => $arm_mollie_webhookurl,
                            'metadata'      => array(
                                'plan_id'           => $plan_id,
                                'entry_id'          => $entry_id,
                                'user_email'        => $user_email_add,
                                'form_id'           => $form_id,
                                'plan_payment_type' => $plan_payment_type,
                                'cst_id'            => $arm_mollie_customer_id
                            ),
                        );

                        $arm_mollie_payment = $arm_mollie->payments->create($arm_first_payment_params);
                        $arm_mollie_checkout_url = !empty($arm_mollie_payment->_links->checkout) ? $arm_mollie_payment->_links->checkout->href : '';
                    } 
                    catch (Exception $e) {
                        $actual_message = $e->getMessage();
                        // $e->error = "1";
                        do_action('arm_payment_log_entry', 'mollie', 'Error while submitting one time payment form request', 'armember', $actual_message, $arm_debug_payment_log_id);
                    }                    
                }

                if(!empty($arm_mollie_checkout_url))
                {
                    $redirect = '<script data-cfasync="false" type="text/javascript" language="javascript">window.location.href="'.$arm_mollie_checkout_url.'";</script>';
                    $return = array('status' => 'success', 'type' => 'redirect', 'message' => $redirect);
                    echo json_encode($return);
                    die;
                } else {
                    $err_msg = isset($arm_global_settings->common_message['arm_payment_fail_mollie']) ? $arm_global_settings->common_message['arm_payment_fail_mollie'] : '';
                    $err_msg = (!empty($err_msg)) ? $err_msg : __('Sorry something went wrong while processing payment with Mollie', 'ARM_MOLLIE');
                    
                    if(!empty($arm_mollie_payment->message))
                    {
                        $err_msg = $arm_mollie_payment->message;
                    }
                    $payment_done = array('status' => FALSE, 'error' => $err_msg);
                    
                    return $payment_done;
                }
            }
        }      
    }
    
    function armmollie_get_remote_post_params($plugin_info = "") {
            global $wpdb;
    
            $action = "";
            $action = $plugin_info;
    
            if (!function_exists('get_plugins')) {
                require_once(ABSPATH . 'wp-admin/includes/plugin.php');
            }
            $plugin_list = get_plugins();
            $site_url = home_url();
            $plugins = array();
    
            $active_plugins = get_option('active_plugins');
    
            foreach ($plugin_list as $key => $plugin) {
                $is_active = in_array($key, $active_plugins);
    
                //filter for only armember ones, may get some others if using our naming convention
                if (strpos(strtolower($plugin["Title"]), "armembermollie") !== false) {
                    $name = substr($key, 0, strpos($key, "/"));
                    $plugins[] = array("name" => $name, "version" => $plugin["Version"], "is_active" => $is_active);
                }
            }
            $plugins = json_encode($plugins);
    
            //get theme info
            $theme = wp_get_theme();
            $theme_name = $theme->get("Name");
            $theme_uri = $theme->get("ThemeURI");
            $theme_version = $theme->get("Version");
            $theme_author = $theme->get("Author");
            $theme_author_uri = $theme->get("AuthorURI");
    
            $im = is_multisite();
            $sortorder = get_option("armSortOrder");
    
            $post = array("wp" => get_bloginfo("version"), "php" => phpversion(), "mysql" => $wpdb->db_version(), "plugins" => $plugins, "tn" => $theme_name, "tu" => $theme_uri, "tv" => $theme_version, "ta" => $theme_author, "tau" => $theme_author_uri, "im" => $im, "sortorder" => $sortorder);
    
            return $post;
        }
    
    function arm_change_pending_gateway_outside($getway, $plan_ID, $user_id){
        $getway[] = 'mollie';        
        return $getway;
    }

    function arm_get_customer_id_from_transaction($arm_transaction_id)
    {
        global $wpdb, $ARMember, $arm_debug_payment_log_id;
        $arm_customer_id = "";
        if(!empty($arm_transaction_id) && (strpos($arm_transaction_id, 'tr_') !== false))
        {
            $arm_mollie = $this->arm_load_mollie_lib();
            $gateway_options = get_option('arm_payment_gateway_settings');
            $pgoptions = maybe_unserialize($gateway_options);
            $pgoptions = $pgoptions['mollie'];

            $is_sandbox_mode = ($pgoptions['mollie_payment_mode'] == "sandbox") ? 1 : 0;
            $ARM_Mollie_API_KEY = ( $is_sandbox_mode ) ? $pgoptions['mollie_sandbox_api_key'] : $pgoptions['mollie_api_key'];
            $arm_mollie->setApiKey($ARM_Mollie_API_KEY);

            try{
                $arm_get_payment_data = $arm_mollie->payments->get($arm_transaction_id);
                $arm_customer_id = !empty($arm_get_payment_data->customerId) ? $arm_get_payment_data->customerId : '';
            }catch(Exception $e){
                do_action('arm_payment_log_entry', 'mollie', 'Error while getting customer from transaction id', 'payment_gateway', $e->getMessage(), $arm_debug_payment_log_id);
            }
        }
        return $arm_customer_id;
    }
    
    function arm2_mollie_cancel_subscription($user_id, $plan_id){
        global $wpdb, $ARMember, $arm_subscription_cancel_msg, $arm_debug_payment_log_id;

        $cancel_debug_data = array(
            'user_id' => $user_id,
            'plan_id' => $plan_id,
        );
        do_action('arm_payment_log_entry', 'mollie', 'Mollie cancel subscription debug data', 'armember', $cancel_debug_data, $arm_debug_payment_log_id);

        if(!empty($user_id) && !empty($plan_id)) 
        { 
            $arm_cancel_subscription_data = array();
            $arm_cancel_subscription_data = apply_filters('arm_gateway_cancel_subscription_data', $arm_cancel_subscription_data, $user_id, $plan_id, 'mollie', 'id', '', '');
            do_action('arm_payment_log_entry', 'mollie', 'Mollie cancel subscription data', 'armember', $arm_cancel_subscription_data, $arm_debug_payment_log_id);

            $planData = !empty($arm_cancel_subscription_data['arm_plan_data']) ? $arm_cancel_subscription_data['arm_plan_data'] : array();
            $arm_user_payment_gateway = !empty($planData['arm_user_gateway']) ? $planData['arm_user_gateway'] : '';

            if(strtolower($arm_user_payment_gateway) == "mollie")
            {
                $arm_payment_mode = !empty($arm_cancel_subscription_data['arm_payment_mode']) ? $arm_cancel_subscription_data['arm_payment_mode'] : 'manual_subscription';

                $arm_payment_log_data = $arm_cancel_subscription_data['arm_payment_log_data'];
                
                $arm_subscr_id = !empty($arm_cancel_subscription_data['arm_subscr_id']) ? $arm_cancel_subscription_data['arm_subscr_id'] : '';

                $arm_customer_id = !empty($arm_cancel_subscription_data['arm_customer_id']) ? $arm_cancel_subscription_data['arm_customer_id'] : '';
                if(empty($arm_customer_id))
                {
                    $arm_customer_id = $arm_payment_log_data->arm_token;
                }

                $arm_transaction_id = !empty($arm_cancel_subscription_data['arm_transaction_id']) ? $arm_cancel_subscription_data['arm_transaction_id'] : '';
                if(empty($arm_transaction_id))
                {
                    $arm_transaction_id = !empty($arm_payment_log_data->arm_transaction_id) ? $arm_payment_log_data->arm_transaction_id : '';
                }

                if(strpos($arm_customer_id, "cst_") == false)
                {
                    $arm_customer_id = $this->arm_get_customer_id_from_transaction($arm_transaction_id);
                    do_action('arm_payment_log_entry', 'mollie', 'Mollie get customer id from transaction id', 'armember', array('customer_id' => $arm_customer_id, 'transaction_id' => $arm_transaction_id), $arm_debug_payment_log_id);
                }

                $arm_cancel_amount = !empty($arm_cancel_subscription_data['arm_cancel_amount']) ? $arm_cancel_subscription_data['arm_cancel_amount'] : 0;

                if($arm_payment_mode == "auto_debit_subscription"){
                    $cancel_subscription = $this->arm_cancel_subscription_immediately($arm_subscr_id, $arm_customer_id);
                    do_action('arm_payment_log_entry', 'mollie', 'Mollie cancel subscription response', 'armember', $cancel_subscription, $arm_debug_payment_log_id);

                    if(!empty($arm_subscription_cancel_msg))
                    {
                        return;
                    }
                }

                do_action('arm_cancel_subscription_payment_log_entry', $user_id, $plan_id, 'mollie', $arm_subscr_id, $arm_transaction_id, $arm_customer_id, $arm_payment_mode, $arm_cancel_amount);
            }
        }
    }
    
    function arm_mollie_currency_support($notAllow, $currency){
        global $arm_payment_gateways;
        if (!array_key_exists($currency, $arm_payment_gateways->currency['mollie'])) {
            $notAllow[] = 'mollie';
        }
        return $notAllow;
    }
    
    function arm2_mollie_webhook(){
        if (isset($_REQUEST['arm-listener']) && in_array($_REQUEST['arm-listener'], array('arm_mollie_wh_api', 'arm_mollie_api')))
        {
            global $wpdb, $ARMember, $arm_global_settings, $arm_subscription_plans, $arm_member_forms, $arm_payment_gateways, $arm_manage_coupons, $payment_done, $arm_debug_payment_log_id, $arm_transaction;

            do_action('arm_payment_log_entry', 'mollie', 'Mollie webhook response', 'payment_gateway', json_encode($_REQUEST), $arm_debug_payment_log_id);

            $arm_mollie = $this->arm_load_mollie_lib();

            do_action('arm_payment_log_entry', 'mollie', 'Webhook data', 'payment_gateway', $_REQUEST, $arm_debug_payment_log_id);
            
            if(isset($_POST['id']) && $_POST['id'] != '')//phpcs:ignore
            {
                $all_payment_gateways = $arm_payment_gateways->arm_get_active_payment_gateways();
                if (isset($all_payment_gateways['mollie']) && !empty($all_payment_gateways['mollie'])) {
                    $options = $all_payment_gateways['mollie'];
                    $mollie_payment_mode = $options['mollie_payment_mode'];
                    $is_sandbox_mode = $mollie_payment_mode == "sandbox" ? true : false;
                    $ARM_Mollie_API_KEY = ( $is_sandbox_mode ) ? $options['mollie_sandbox_api_key'] : $options['mollie_api_key'];
                    $arm_mollie->setApiKey($ARM_Mollie_API_KEY);
                    
                    $transaction_id = $_POST['id'];//phpcs:ignore

                    $arm_mollie_get_transient = get_transient("arm_mollie_transaction_".$transaction_id);

                    if(false == $arm_mollie_get_transient || $arm_mollie_get_transient == 'false' || empty($arm_mollie_get_transient)) 
                    {
                        set_transient("arm_mollie_transaction_".$transaction_id, $transaction_id, DAY_IN_SECONDS);
                    }
                    else
                    {
                        do_action('arm_payment_log_entry', 'mollie', 'Mollie duplicate transaction id', 'payment_gateway', $transaction_id, $arm_debug_payment_log_id);
                        die();
                    }
                
                    $payment = $arm_mollie->payments->get($transaction_id);

                    if(!empty($payment->amountChargedBack) || $payment->amountChargedBack != null )
                    {
                        do_action('arm_payment_log_entry', 'mollie', 'Mollie webhook chargeback response', 'payment_gateway', json_encode($payment->amountChargedBack), $arm_debug_payment_log_id);

                        if(!empty($payment->amountChargedBack->value))
                        {
                            $subscriptionId = $payment->subscriptionId;
                            //$transaction_id = $payment->id;

                            $arm_success_payment_record = $wpdb->get_row( $wpdb->prepare("SELECT arm_log_id, arm_user_id, arm_plan_id FROM `" . $ARMember->tbl_arm_payment_log . "` WHERE `arm_transaction_id`=%s AND `arm_transaction_id`!=%s AND `arm_payment_gateway`=%s AND arm_transaction_status = %s ORDER BY `arm_log_id` DESC",$transaction_id,'','mollie','success'), ARRAY_A);

                            do_action('arm_payment_log_entry', 'mollie', 'Mollie webhook chargeback get user data by transaction ID', 'payment_gateway', json_encode($arm_success_payment_record), $arm_debug_payment_log_id);

                            if(!empty($arm_success_payment_record)){
                                $entry_plan = $arm_success_payment_record['arm_plan_id'];
                                $user_id = $arm_success_payment_record['arm_user_id'];
                                $arm_current_date_time = current_time('timestamp');
                                $arm_user_meta_data = get_user_meta($user_id, 'arm_user_plan_'.$entry_plan, true);
                                if(!empty($arm_user_meta_data)){
                                    $arm_user_meta_data['arm_next_due_payment'] = $arm_current_date_time;
                                    $arm_user_meta_data['arm_completed_recurring'] = $arm_user_meta_data['arm_completed_recurring']-1;
                                    $arm_is_user_in_grace = isset($arm_user_meta_data['arm_is_user_in_grace']) ? $arm_user_meta_data['arm_is_user_in_grace'] : 0;

                                    update_user_meta($user_id, 'arm_user_plan_'.$entry_plan, $arm_user_meta_data);
                                }

                                $arm_new_transaction_id = $transaction_id." - ".esc_html__('Failed', 'ARM_MOLLIE');
                                $wpdb->update($ARMember->tbl_arm_payment_log, array('arm_transaction_status' => 'failed', 'arm_transaction_id' => $arm_new_transaction_id), array('arm_log_id' => $arm_success_payment_record['arm_log_id']));
                                
                                $arm_subscription_plans->arm_user_plan_status_action(array('plan_id' => $entry_plan, 'user_id' => $user_id, 'action' => 'failed_payment'), true);
                                
                                $arm_manage_communication->arm_user_plan_status_action_mail(array('plan_id' => $entry_plan, 'user_id' => $user_id, 'action' => 'failed_payment'));
                            }

                            
                            
                        }
                        die();
                    }

                    do_action('arm_payment_log_entry', 'mollie', 'Mollie webhook payment response', 'payment_gateway', json_encode($payment), $arm_debug_payment_log_id);

                    $arm_card_number = !empty($payment->details->cardNumber) ? $payment->details->cardNumber : '';
                    if(!empty($arm_card_number)){
                        $arm_card_number = $arm_transaction->arm_mask_credit_card_number($arm_card_number);
                    }
                    
                    $arm_cust_id = !empty($payment->customerId) ? $payment->customerId : '';
                    $arm_subs_id = !empty($payment->subscriptionId) ? $payment->subscriptionId : '';
                    if(!empty($arm_cust_id) && !empty($arm_subs_id)){
                        $get_subscriptions_customer = $arm_mollie->customers->get($arm_cust_id);
                        $armGetSubsDetails = $get_subscriptions_customer->getSubscription($arm_subs_id);

                        if(empty($armGetSubsDetails) || empty($armGetSubsDetails->status) || (isset($armGetSubsDetails->status) && $armGetSubsDetails->status != "active")){
                            die;
                        }
                    }

                    $arm_token = $arm_subs_id;
                    $entry_id = !empty($payment->metadata->entry_id) ? $payment->metadata->entry_id : '';
                    $entry_email = !empty($payment->metadata->user_email) ? $payment->metadata->user_email : '';
                    $arm_log_plan_id = !empty($payment->metadata->plan_id) ? $payment->metadata->plan_id : '';
                    $arm_log_amount = !empty($payment->amount) ? $payment->amount : 0;

                    if(empty($entry_id) && !empty($arm_subs_id))
                    {
                        $payment_log_data = $wpdb->get_row("SELECT * FROM `".$ARMember->tbl_arm_payment_log."` WHERE `arm_token`='".$arm_subs_id."' OR arm_transaction_id = '".$arm_subs_id."' ORDER BY arm_log_id DESC LIMIT 1", ARRAY_A); //phpcs:ignore
                        if(!empty($payment_log_data))
                        {
                            $entry_email = $payment_log_data['arm_payer_email'];
                            $arm_log_plan_id = $payment_log_data['arm_plan_id'];
                            $arm_log_amount = $payment_log_data['arm_amount'];

                            $entry_data = $wpdb->get_row("SELECT `arm_entry_id`, `arm_entry_email`, `arm_entry_value`, `arm_form_id`, `arm_user_id`, `arm_plan_id` FROM `".$ARMember->tbl_arm_entries."` WHERE `arm_entry_email`='".$entry_email."' AND `arm_plan_id`='".$arm_log_plan_id."' order by arm_entry_id desc", ARRAY_A);//phpcs:ignore

                            $entry_id = !empty($entry_data['arm_entry_id']) ? $entry_data['arm_entry_id'] : 0;
                        }
                    }

                    $arm_payment_type = !empty($payment->metadata->plan_payment_type) ? $payment->metadata->plan_payment_type : '';

                    $cst_id = !empty($payment->metadata->cst_id) ? $payment->metadata->cst_id : '';

                    $payment_status = !empty($payment->status) ? $payment->status : '';
                    
                    if(isset($payment) && $payment_status == 'paid' && empty($arm_subs_id))
                    {
                        if(!empty($payment->metadata->recurring_payment))
                        {
                            //If transaction is first time subscription then subscription created for customer
                            $arm_mollie_webhookurl = $arm_global_settings->add_query_arg("arm-listener", "arm_mollie_api", get_home_url() . "/");

                            $currency = $arm_payment_gateways->arm_get_global_currency();

                            $entry_data = $wpdb->get_row("SELECT * FROM `".$ARMember->tbl_arm_entries."` WHERE `arm_entry_id`='".$entry_id."'", ARRAY_A);//phpcs:ignore

                            $entry_values = maybe_unserialize($entry_data['arm_entry_value']);

                            do_action('arm_payment_log_entry', 'mollie', 'Webhook Entry Value', 'payment_gateway', $entry_values, $arm_debug_payment_log_id);

                            $arm_selected_payment_cycle = $entry_values['arm_selected_payment_cycle'];
                            
                            $arm_return_data = array();
                            $arm_return_data = apply_filters('arm_calculate_payment_gateway_submit_data', $arm_return_data, 'mollie', $options, $entry_values, $entry_id);
                            
                            do_action('arm_payment_log_entry', 'mollie', 'Webhook arm_return_data', 'payment_gateway', $arm_return_data, $arm_debug_payment_log_id);

                            $arm_plan_obj = $wpdb->get_row("SELECT * FROM `".$ARMember->tbl_arm_subscription_plans."` WHERE `arm_subscription_plan_id`='".$arm_log_plan_id."'", ARRAY_A);//phpcs:ignore

                            $plan_name = $arm_plan_obj['arm_subscription_plan_name'];

                            $arm_subscription_plan_options = maybe_unserialize($arm_plan_obj['arm_subscription_plan_options']);

                            $arm_selected_cycle_data = $arm_subscription_plan_options['payment_cycles'][$arm_selected_payment_cycle];

                            $arm_setup_id = $entry_values['setup_id'];

                            $arm_recurring_type = $arm_selected_cycle_data['billing_type'];
                            $arm_recurring_time = $arm_selected_cycle_data['recurring_time'];
                            $arm_recurring_billing_cycle = $arm_selected_cycle_data['billing_cycle'];
                            $arm_subs_start_date = "";
                            $arm_recurring_interval = "";
                            if($arm_recurring_type == "D"){
                                $arm_recurring_interval = $arm_recurring_billing_cycle." days";
                            }else if($arm_recurring_type == "W"){
                                $arm_recurring_interval = $arm_recurring_billing_cycle." weeks";
                            }else if($arm_recurring_type == "M"){
                                $arm_recurring_interval = $arm_recurring_billing_cycle." months";
                            }else if($arm_recurring_type == "Y"){
                                $arm_recurring_interval = "12 months";
                            }

                            $arm_current_time = current_time('mysql');
                            $arm_subs_start_date = date('Y-m-d', strtotime("+".$arm_recurring_interval, strtotime($arm_current_time)));
			    
			    do_action('arm_payment_log_entry', 'mollie', 'Webhook Entry Value', 'payment_gateway', $entry_values, $arm_debug_payment_log_id);

                            $arm_tax_percentage = isset($entry_values['arm_common_tax_amount']) ? $entry_values['arm_common_tax_amount'] : 0;
                            
                            $discount_amt = floatval(str_replace(',', '', $entry_values['arm_total_payable_amount']));
                            $discount_amt = number_format($discount_amt,2,".","");
							
                            //update plan amount if plan has trial                            
                            
                            $plan_cycle = !empty($entry_values['arm_payment_cycle_plan_'.$arm_log_plan_id]) ? $entry_values['arm_payment_cycle_plan_'.$arm_log_plan_id] : 0;
                            do_action('arm_payment_log_entry', 'mollie', 'Create subscription In members Trial original', 'payment_gateway', $discount_amt, $arm_debug_payment_log_id);                           
                            $arm_trial_timestamp = !empty($user_plan_detail['arm_trial_end']) ? $user_plan_detail['arm_trial_end'] : '';
                            if(empty($arm_trial_timestamp) && !empty($arm_return_data['arm_recurring_data']['trial']))
                            {
                                $trial_type = $arm_subscription_plan_options['trial']['type'];
                                $trial_days = $arm_subscription_plan_options['trial']['days'];
                                $trial_months = $arm_subscription_plan_options['trial']['months'];
                                $trial_years = $arm_subscription_plan_options['trial']['years'];
                                if($trial_type == "D"){
                                    $arm_trial_interval = $trial_days." days";
                                }else if($trial_type == "M"){
                                    $arm_trial_interval = $trial_months." months";
                                }else if($trial_type == "Y"){
                                    $arm_trial_interval = $trial_years." years";
                                }
                                else{
                                    $arm_trial_interval = $trial_days." days";
                                }
                                $arm_current_time = current_time('mysql');
                                $arm_subs_start_date = date('Y-m-d', strtotime("+".$arm_trial_interval, strtotime($arm_current_time)));
                            }
                            
                            do_action('arm_payment_log_entry', 'mollie', 'Create subscription In members Trial end date', 'payment_gateway', $arm_subs_start_date, $arm_debug_payment_log_id);

                            if(!empty($entry_values['arm_coupon_code']))
                            {
                                $arm_user_old_plan = (isset($entry_values['arm_user_old_plan']) && !empty($entry_values['arm_user_old_plan'])) ? explode(",", $entry_values['arm_user_old_plan']) : array();

                                $arm_coupon_code = $entry_values['arm_coupon_code'];
                                $couponApply = $arm_manage_coupons->arm_apply_coupon_code($arm_coupon_code, $arm_log_plan_id, $arm_setup_id, $arm_selected_payment_cycle, $arm_user_old_plan);

                                do_action('arm_payment_log_entry', 'mollie', 'Create subscription coupon data', 'payment_gateway', $couponApply, $arm_debug_payment_log_id);

                                $arm_coupon_on_each_subscriptions = !empty($couponApply['arm_coupon_on_each_subscriptions']) ? $couponApply['arm_coupon_on_each_subscriptions'] : 0;

                                if(empty($couponApply['arm_coupon_on_each_subscriptions']))
                                {
                                    //If coupon on each subscription option not checked then re-calculate subscription price.
                                    $discount_amt = number_format($arm_plan_obj['arm_subscription_plan_amount'],2,".","");

                                    if(!empty($arm_tax_percentage))
                                    {
                                        $arm_tax_amount = ($discount_amt * $arm_tax_percentage) / 100;
                                        $discount_amt = $discount_amt + $arm_tax_amount;

                                        $discount_amt = number_format((float)$discount_amt, 2);
                                        $discount_amt = str_replace(',', '', $discount_amt);
                                    }
                                }
                            }

                            try{
                                $arm_create_subscription_params = array(
                                    'amount'       => array(
                                        'currency' => $currency,
                                        'value'    => $discount_amt,
                                    ),
                                    'interval'    => $arm_recurring_interval,
                                    'method'      => NULL,
                                    'startDate'   => $arm_subs_start_date,
                                    'description'   => current_time('mysql').' '.$plan_name,
                                    'webhookUrl'   => $arm_mollie_webhookurl,
                                );

                                if($arm_recurring_time != "infinite"){
                                    $arm_create_subscription_params['times'] = $arm_recurring_time - 1;
                                }

                                do_action('arm_payment_log_entry', 'mollie', 'Create subscription params', 'payment_gateway', $arm_create_subscription_params, $arm_debug_payment_log_id);

                                $arm_mollie_customer_obj = $arm_mollie->customers->get($arm_cust_id);
                                $arm_create_customer_subscription = $arm_mollie_customer_obj->createSubscription($arm_create_subscription_params);

                                if(!empty($arm_create_customer_subscription->id))
                                {
                                    $arm_create_customer_subscription_arr = json_decode(json_encode($arm_create_customer_subscription), TRUE);

                                    do_action('arm_payment_log_entry', 'mollie', 'Created subscription response', 'payment_gateway', $arm_create_customer_subscription_arr, $arm_debug_payment_log_id);

                                    $arm_subs_id = $arm_create_customer_subscription_arr['id'];
                                    $arm_token = $arm_create_customer_subscription_arr['id'];

                                    $arm_create_customer_subscription_arr['transaction_id'] = $transaction_id;

                                    if(!empty($arm_card_number)){
                                        $arm_create_customer_subscription_arr['arm_payment_card_number'] = $arm_card_number;
                                    }

                                    $arm_subscription_field_name = 'id';
                                    $arm_token_field_name = 'id';
                                    $arm_transaction_id_field_name = 'transaction_id';

                                    $arm_webhook_save_membership_data = array();
                                    $arm_webhook_save_membership_data = apply_filters('arm_modify_payment_webhook_data', $arm_webhook_save_membership_data, $arm_create_customer_subscription_arr, 'mollie', $arm_token, $transaction_id, $entry_id, $arm_subs_id, $arm_subscription_field_name, $arm_token_field_name, $arm_transaction_id_field_name);
                                }

                            }catch(Exception $e){
                                do_action('arm_payment_log_entry', 'mollie', 'Error while creating subscription', 'payment_gateway', $e->getMessage(), $arm_debug_payment_log_id);
                            }
                        }
                        else
                        {
                            //Condition for onetime payment.
                            $arm_subs_id = '';
                            $arm_payment_gateway_data = array();
                            $arm_payment_gateway_data['transaction_id'] = $transaction_id;
                            $arm_payment_gateway_data['id'] = $arm_subs_id;
                            $arm_payment_gateway_data['customer_id'] = $arm_cust_id;

                            if(!empty($arm_card_number)){
                                $arm_payment_gateway_data['arm_payment_card_number'] = $arm_card_number;
                            }

                            $arm_subscription_field_name = 'id';
                            $arm_token_field_name = 'id';
                            $arm_transaction_id_field_name = 'transaction_id';

                            $arm_webhook_save_membership_data = array();
                            $arm_webhook_save_membership_data = apply_filters('arm_modify_payment_webhook_data', $arm_webhook_save_membership_data, $arm_payment_gateway_data, 'mollie', $arm_token, $transaction_id, $entry_id, $arm_subs_id, $arm_subscription_field_name, $arm_token_field_name, $arm_transaction_id_field_name);
                        }
                    }
                    else if(!empty($arm_subs_id) && $payment_status == 'paid')
                    {
                        $arm_token = $arm_subs_id;
                        $arm_payment_gateway_data = array();
                        $arm_payment_gateway_data['transaction_id'] = $transaction_id;
                        $arm_payment_gateway_data['id'] = $arm_subs_id;
                        $arm_payment_gateway_data['customer_id'] = $arm_cust_id;

                        if(!empty($arm_card_number)){
                            $arm_payment_gateway_data['arm_payment_card_number'] = $arm_card_number;
                        }

                        $arm_subscription_field_name = 'id';
                        $arm_token_field_name = 'id';
                        $arm_transaction_id_field_name = 'transaction_id';

                        $arm_webhook_save_membership_data = array();
                        $arm_webhook_save_membership_data = apply_filters('arm_modify_payment_webhook_data', $arm_webhook_save_membership_data, $arm_payment_gateway_data, 'mollie', $arm_token, $transaction_id, $entry_id, $arm_subs_id, $arm_subscription_field_name, $arm_token_field_name, $arm_transaction_id_field_name);
                    }
                }
            }
        }
    }
    
    function arm_mollie_modify_coupon_code($data,$payment_mode,$couponData,$planAmt,$planObj){
        
        if( isset($planObj->type) && 'recurring' == $planObj->type && $payment_mode == 'auto_debit_subscription' ){
            if( $data['status'] == 'success' && $data['total_amt'] <= 0 ){
                $coupon_amt = $planAmt - 0.10;
                $data['coupon_amt'] = $coupon_amt;
                $data['total_amt'] = 0.10;
            }
        }
        return $data;
    }
    
    function arm_mollie_change_pending_gateway_outside($user_pending_pgway,$plan_ID,$user_id){
        global $is_free_manual,$ARMember;
        if( $is_free_manual ){
            $key = array_search('mollie',$user_pending_pgway);
            unset($user_pending_pgway[$key]);
        }
        return $user_pending_pgway;
    }

    function arm_get_mollie_payment_log_from_subscription($arm_cst_id = '', $arm_subs_id = ''){
        global $ARMember, $wpdb, $arm_payment_gateways, $arm_debug_payment_log_id;
        $arm_payment_log_data = array();
        $all_payment_gateways = $arm_payment_gateways->arm_get_active_payment_gateways();
        if (isset($all_payment_gateways['mollie']) && !empty($all_payment_gateways['mollie'])) {
            $options = $all_payment_gateways['mollie'];
            $mollie_payment_mode = $options['mollie_payment_mode'];
            $is_sandbox_mode = $mollie_payment_mode == "sandbox" ? true : false;
            $ARM_Mollie_API_KEY = ( $is_sandbox_mode ) ? $options['mollie_sandbox_api_key'] : $options['mollie_api_key'];

            $arm_payment_log_data = array();
            if(!empty($arm_cst_id) && !empty($arm_subs_id))
            {
                try{
                    $get_subscription_payment_details = "https://api.mollie.com/v2/customers/".$arm_cst_id."/payments";
                    
                    $headers = array(
                        'Content-Type: application/json',
                        'Authorization: Bearer '.$ARM_Mollie_API_KEY,
                    );

                    $curl = curl_init();
                    curl_setopt($curl, CURLOPT_URL, $get_subscription_payment_details);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
                    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                    $response = curl_exec($curl);
                    $response = json_decode($response, TRUE);
                    curl_close($curl);

                    if(!empty($response) && !empty($response['_embedded']['payments']))
                    {
                        $subscription_payment_data = $response['_embedded']['payments'];
                        $arm_trans_ids = '';
                        foreach($subscription_payment_data as $subscription_payment_key => $subscription_payment_val)
                        {
                            $arm_trans_ids .= !empty($arm_trans_ids) ? ",'".$subscription_payment_val['id']."'" : "'".$subscription_payment_val['id']."'";
                        }

                        do_action('arm_payment_log_entry', 'mollie', 'Get transaction ids from subscription payments', 'payment_gateway', $arm_trans_ids, $arm_debug_payment_log_id);

                        if(!empty($arm_trans_ids))
                        {
                            $arm_payment_log_data = $wpdb->get_row("SELECT * FROM `".$ARMember->tbl_arm_payment_log."` WHERE arm_transaction_id IN (".$arm_trans_ids.") ORDER BY arm_log_id DESC LIMIT 1");//phpcs:ignore

                            do_action('arm_payment_log_entry', 'mollie', 'Payment log data from subscription payments', 'payment_gateway', $arm_payment_log_data, $arm_debug_payment_log_id);
                        }
                    }
                }catch(Exception $e){
                    do_action('arm_payment_log_entry', 'mollie', 'Error while getting subscription data from payments', 'payment_gateway', $e->getMessage(), $arm_debug_payment_log_id);
                }
            }
        }

        return $arm_payment_log_data;
    }
}

global $arm_mollie;
$arm_mollie = new ARM_Mollie();

add_filter('arm_addon_license_content_external', 'armmollie_addon_license_content_external', 10, 1);
function armmollie_addon_license_content_external($arm_license_addon_content){
    $armmollie_license_status=get_option('armmollie_license_status');
    $armmollie_license_key=get_option('armmollie_license_key');
    $arm_license_deactive=0;
    $arm_license_active_style='display:inline';
    $arm_license_deactive_style='display:none;';
    if($armmollie_license_status=='valid' && !empty($armmollie_license_key)){
        $arm_license_deactive=1;
        $arm_license_active_style='display:none';
        $arm_license_deactive_style='display:inline';        
    }    
    $arm_license_addon_content .='<form method="post" action="#" id="armmollie_license_settings" class="arm_license_settings arm_admin_form" onsubmit="return false;">
        <div class="arm_feature_settings_container arm_feature_settings_wrapper" style="margin-top:30px;">
            <div class="page_sub_title">'.__('Activate Mollie payment gateway Addon','ARM_MOLLIE').'</div>            
            <table class="form-table">
                <tr class="form-field">
                    <th class="arm-form-table-label">'.__('License Code', 'ARM_MOLLIE').'</th>
                    <td class="arm-form-table-content">';
                    
                        $arm_license_addon_content .='<div id="licenseactivatedmessage" class="armmollie_remove_license_section" style="width:300px; vertical-align:top;padding:5px;'.$arm_license_deactive_style.'">'.$armmollie_license_key.'</div>';
                    
                        $arm_license_addon_content .='<div class="armmollie_add_license_section" style="'.$arm_license_active_style.'"><input type="text" name="arm_license_key" id="arm_license_key" placeholder="Enter License Code" value="'.$armmollie_license_key.'" autocomplete="off" /><div class="arperrmessage" id="arm_license_key_error" style="display:none;">'.__('This field cannot be blank.', 'ARM_MOLLIE').'</div></div>'; 

                    $arm_license_addon_content .='</td>
                </tr>
                <tr class="form-field">
                    <th class="arm-form-table-label">&nbsp;</th>
                    <td class="arm-form-table-content">';
                    $arm_license_addon_content .='<input type="hidden" name="arm_license_deactive" id="arm_license_deactive" value="'.$arm_license_deactive.'" />';
                        $arm_license_addon_content .='<span id="license_link" class="armmollie_remove_license_section" style="'.$arm_license_deactive_style.'"><button type="button" id="armmollie-remove-verify-purchase-code" name="armmollie_remove_license" style="width:170px; border:0px; color:#FFFFFF; height:40px; border-radius:6px;" class="red_remove_license_btn">'.__('Remove License', 'ARM_MOLLIE').'</button></span>';

                        $arm_license_addon_content .='<span id="license_link" class="armmollie_add_license_section" style="'.$arm_license_active_style.'"><button type="button" id="armmollie-verify-purchase-code" name="armmollie_activate_license" style="width:180px; border:0px; color:#FFFFFF; height:40px; border-radius:3px;cursor:pointer;line-height: 15px;" class="greensavebtn">'.__('Activate License', 'ARM_MOLLIE').'</button></span>';
                    
                    $arm_license_addon_content .='<span id="license_loader" style="display:none;">&nbsp;<img src="'.MEMBERSHIP_IMAGES_URL.'/loading_activation.gif" height="15" /></span>
                        <span id="license_error" style="display:none;">&nbsp;</span>
                        <span id="license_reset" style="display:none;">&nbsp;&nbsp;<a onclick="javascript:return false;" href="#">Click here to submit RESET request</a></span>
                        <span id="license_success" style="display:none;">'.__('License Activated Successfully.', 'ARM_MOLLIE').'</span>                        
                    </td>
                </tr>

            </table>
        </div>
    </form>
    <div class="armclear"></div>';    
     
    global $arm_global_settings;
    /* **********./Begin remove License Popup/.********** */
    $arm_remove_license_popup_content = '<span class="arm_confirm_text">'.__("Are you sure you want to Remove this License?",'ARM_MOLLIE' );		
    $arm_remove_license_popup_content .= '<input type="hidden" value="" id="armmollie_remove_license_flag"/>';
    $arm_remove_license_popup_title = '<span class="arm_confirm_text">'.__('Remove License', 'ARM_MOLLIE').'</span>';		
    
    $arm_remove_license_popup_arg = array(
        'id' => 'armmollie_remove_license_form_message',
        'class' => 'armmollie_remove_license_form_message',
        'title' => $arm_remove_license_popup_title,
        'content' => $arm_remove_license_popup_content,
        'button_id' => 'armmollie_remove_license_ok_btn',
        'button_onclick' => "armmollie_deactivate_license();",
    );
    $arm_license_addon_content .=$arm_global_settings->arm_get_bpopup_html($arm_remove_license_popup_arg);
    /* **********./End remove License Popup/.********** */
		
    return $arm_license_addon_content;
}

add_action('wp_ajax_armmollieactivatelicense','armmollie_license_update');
function armmollie_license_update(){
    global $wp, $wpdb, $ARMember, $arm_capabilities_global,$arm_slugs;
    
    $response = array('type' => 'error', 'msg' => __('Sorry, Something went wrong. Please try again.', 'ARM_MOLLIE'),'arm_license_status'=>0);
    
    /*check the license page capabilities to user */
    $arm_manage_license_key = !isset(($arm_capabilities_global['arm_manage_license']))?$arm_slugs->licensing:$arm_capabilities_global['arm_manage_license'];
    $ARMember->arm_check_user_cap($arm_manage_license_key, '1', '1');

    if(isset($_POST['arm_license_key']) && !empty($_POST['arm_license_key'])){ //phpcs:ignore
        $arm_license_deactive=(isset($_POST['arm_license_deactive']))?$_POST['arm_license_deactive']:'0'; //phpcs:ignore
        // activate license for this addon
        $posted_license_key = trim($_POST['arm_license_key']); //phpcs:ignore
        $posted_license_package = '1803';
        $edd_action='activate_license';
        if($arm_license_deactive=='1'){
            $edd_action='deactivate_license';
            $posted_license_key=get_option('armmollie_license_key');
        }

        $api_params = array(
            'edd_action' => $edd_action,
            'license'    => $posted_license_key,
            'item_id'  => $posted_license_package,
            // 'url'        => home_url()
        );

        if($edd_action!='deactivate_license'){
            $api_params['url']=home_url();
        }

        // Call the custom API.
        $response = wp_remote_post( ARMADDON_STORE_URL, array( 'timeout' => 15, 'sslverify' => false, 'body' => $api_params ) );
        $response_return = array();
        $message = "";
        if ( is_wp_error( $response ) || 200 !== wp_remote_retrieve_response_code( $response ) ) {
            $message =  ( is_wp_error( $response ) && ! empty( $response->get_error_message() ) ) ? $response->get_error_message() : __( 'An error occurred, please try again.' );
            $response_return['msg']=$message;
            $response_return['arm_license_status']=0;
        } else {
            $license_data = json_decode( wp_remote_retrieve_body( $response ) );
            
            $license_data_string = wp_remote_retrieve_body( $response );
            if ( false === $license_data->success && ! empty( $license_data->error ) ) {
                switch( $license_data->error ) {
                    case 'expired' :
                        $message = sprintf(
                            __( "Your license key expired on %1\$s.", 'ARM_MOLLIE'),
                            date_i18n( get_option( 'date_format' ), strtotime( $license_data->expires, current_time( 'timestamp' ) ) )
                        );
                        break;
                    case 'revoked' :
                        $message = __( 'Your license key has been disabled.', 'ARM_MOLLIE');
                        break;
                    case 'missing' :
                        $message = __( 'Invalid license.', 'ARM_MOLLIE');
                        break;
                    case 'invalid' :
                    case 'site_inactive' :
                        $message = __( 'Your license is not active for this URL.', 'ARM_MOLLIE');
                        break;
                    case 'item_name_mismatch' :
                        $message = __('This appears to be an invalid license key for your selected package.', 'ARM_MOLLIE');
                        break;
                    case 'invalid_item_id' :
                            $message = __('This appears to be an invalid license key for your selected package.', 'ARM_MOLLIE');
                            break;
                    case 'no_activations_left':
                        $message = __( 'Your license key has reached its activation limit.', 'ARM_MOLLIE');
                        break;
                    default :
                        $message = __( 'An error occurred, please try again.', 'ARM_MOLLIE');
                        break;
                }
                $response_return['msg']=$message;
                $response_return['arm_license_status']=0;

            }else if($license_data->license === "valid"){
                update_option('armmollie_license_key', $posted_license_key );
                update_option('armmollie_license_package', $posted_license_package );
                update_option('armmollie_license_status', $license_data->license );
                update_option('armmollie_license_data_activate_response', $license_data_string );

                $message = "License Activated Successfully.";
                $response_return['type']='success';
                $response_return['msg']=$message;
                $response_return['arm_license_status']=1;
            } else if( ( $license_data->license === "deactivated" || $license_data->license === "failed" ) && $arm_license_deactive=='1' ) {
                delete_option('armmollie_license_key');
                delete_option('armmollie_license_package');
                delete_option('armmollie_license_status');
                delete_option('armmollie_license_data_activate_response');
                
                $message = "License Deactivated Successfully.";
                $response_return['type']='success';
                $response_return['msg']=$message;
                $response_return['arm_license_status']=0;
            }    

        }
        
    }    
    echo json_encode($response_return);
    die();
}

if ( ! class_exists( 'armember_pro_updater' ) ) {
	require_once ARM_MOLLIE_CLASSES_DIR . '/class.armember_pro_plugin_updater.php';
}

function armember_mollie_plugin_updater() {
    global $arm_mollie_version;

	$plugin_slug_for_update = 'armembermollie/armembermollie.php';

	// To support auto-updates, this needs to run during the wp_version_check cron job for privileged users.
	$doing_cron = defined( 'DOING_CRON' ) && DOING_CRON;
	if ( ! current_user_can( 'manage_options' ) && ! $doing_cron ) {
		return;
	}

	// retrieve our license key from the DB
	$license_key = trim( get_option( 'armmollie_license_key' ) );
	$package = trim( get_option( 'armmollie_license_package' ) );

	// setup the updater
	$edd_updater = new armember_pro_updater(
		ARMADDON_STORE_URL,
		$plugin_slug_for_update,
		array(
			'version' => $arm_mollie_version,  // current version number
			'license' => $license_key,             // license key (used get_option above to retrieve from DB)
			'item_id' => $package,       // ID of the product
			'author'  => 'Repute Infosystems', // author of this plugin
			'beta'    => false,
		)
	);

}
add_action( 'init', 'armember_mollie_plugin_updater' );