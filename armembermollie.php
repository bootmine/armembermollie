<?php 
/*
Plugin Name: ARMember - Mollie payment gateway Addon
Description: Extension for ARMember plugin to accept payments using Mollie Payment Gateway.
Version: 3.5
Plugin URI: https://www.armemberplugin.com
Author: Repute InfoSystems
Author URI: https://www.armemberplugin.com
Text Domain: ARM_MOLLIE
*/

define('ARM_MOLLIE_DIR_NAME', 'armembermollie');
define('ARM_MOLLIE_DIR', WP_PLUGIN_DIR . '/' . ARM_MOLLIE_DIR_NAME);

if (file_exists( ARM_MOLLIE_DIR . '/autoload.php')) {
    require_once ARM_MOLLIE_DIR . '/autoload.php';
}