<?php

global $armnew_mollie_version;
$armnew_mollie_version = '3.5';
update_option('arm_mollie_version', $armnew_mollie_version);

$arm_version_updated_date_key = 'arm_mollie_version_updated_date_'.$armnew_mollie_version;
$arm_version_updated_date = current_time('mysql');
update_option($arm_version_updated_date_key, $arm_version_updated_date);